package edu.ecpi.benboy4773.InheritanceExample;

public class Boat extends Vehicle {

	public float computeWeight() {
		return (float) 2;
	}

	public void save() {
		System.out.println("Boat Save");
	}

//	public String toString(){
//		return "BOAT!";
//	}

}
