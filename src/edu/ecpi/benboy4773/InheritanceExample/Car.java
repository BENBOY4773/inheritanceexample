package edu.ecpi.benboy4773.InheritanceExample;

public class Car extends Vehicle{

	protected int numberOfDoors;
	
	public int getNumberOfDoors() {
		return numberOfDoors;
	}

	public void setNumberOfDoors(int numberOfDoors) {
		this.numberOfDoors = numberOfDoors;
	}

	public float computeWeight(){
		return (float) 1;
	}

}
