package edu.ecpi.benboy4773.InheritanceExample;

public class Main {


	public static void main(String[] args) {
		int NUM_ITEMS = 4;
		
		Vehicle[] inventory = new Vehicle[NUM_ITEMS];
		
		for ( int i = 0; i < NUM_ITEMS ; i++ ){
			if (i % 2 == 0){
				inventory[i] = new Car();
			}
			else {
				inventory[i] = new Boat();
			}
		}
		
		for ( int i = 0; i < NUM_ITEMS ; i++ ){
			float weight = inventory[i].computeWeight();
//			Class cls = inventory[i].getClass();
//			String simpleName = cls.getSimpleName();
			String simpleName = inventory[i].getClass().getSimpleName(); 
			System.out.println("Location " + i + " is a "+ simpleName + " that weighs " + weight);
			
//			if ( inventory[i] instanceof Car ){
//				System.out.println("Location " + i + " is a car weighing " + weight);
//			}
//			else if ( inventory[i] instanceof Boat ){
//				System.out.println("Location " + i + " is a boat that weighs " + weight);
//			}
			
			inventory[i].save();
		}
		
	}

}
