package edu.ecpi.benboy4773.InheritanceExample;

public interface PersistentObject {
	public void save();
	public Object retrieve();
}
