package edu.ecpi.benboy4773.InheritanceExample;

public abstract class Vehicle implements PersistentObject {
	protected String color;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	public abstract float computeWeight();
	
	public void save() {
		System.out.println("Generic Vehicle Save");
	}

	public Object retrieve() {
		return null;
	}
	
}
